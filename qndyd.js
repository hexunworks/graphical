
var demoWorkspace = Blockly.inject('blocklyDiv', {
    toolbox: document.getElementById('toolbox'),
});
Blockly.Xml.domToWorkspace(document.getElementById('sd'), demoWorkspace);

function copyToClip(content, message) {
    var aux = document.createElement('input');
    aux.setAttribute('value', content);
    document.body.appendChild(aux);
    aux.select();
    document.execCommand('copy');
    document.body.removeChild(aux);
    if (message == null) {
        mdui.snackbar({message:"复制成功！",position: 'top',})
    } else{
        mdui.snackbar({message,position: 'top',})
    }
}
function showjs() {
    Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
    var code = Blockly.JavaScript.workspaceToCode(demoWorkspace);
    var filncode = "(async function(){console.clear();"+code+"})()";
    var x=mdui.snackbar({
        message: filncode,
        timeout: 0,
        position: 'top',
        buttonText: '复制',
        closeOnInsideClick: false,
        onButtonClick:()=>{
            copyToClip(filncode);
            x.close()
        },
    });
}
function wbsjjm() {
    window.open("");
}
async function ptjs(e){
    if(e=='d'){
        function readfile() {
            wks=demoWorkspace
            var e = prompt("请输入文件内容")
            var xml = Blockly.Xml.textToDom(e);
            Blockly.Xml.domToWorkspace(xml, demoWorkspace);
        }
        readfile()
    }else{
        var xml = Blockly.Xml.workspaceToDom(demoWorkspace);
        var xml_text = Blockly.Xml.domToText(xml);
        exportRaw('myblocks.xml',xml_text)
        function fakeClick(obj) {
            var ev = document.createEvent("MouseEvents");
            ev.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            obj.dispatchEvent(ev);
        }

        function exportRaw(name, data) {
            var urlObject = window.URL || window.webkitURL || window;
            var export_blob = new Blob([data]);
            var save_link = document.createElementNS("http://www.w3.org/1999/xhtml", "a")
            save_link.href = urlObject.createObjectURL(export_blob);
            save_link.download = name;
            fakeClick(save_link);
        }
    }
}
